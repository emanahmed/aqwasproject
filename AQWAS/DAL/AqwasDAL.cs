﻿
using AQModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Utility;

namespace DAL
{
    public class AqwasDAL
    {
        #region Catogery
        /// <summary>
        /// To Bind Catogery In DropDownList IN UI
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> GetCategoryListForDropdownlist()
        {
            IEnumerable<SP_Catogery_Get_Result> _list = BindCatogery();
            return _list.Select(e => new SelectListItem()
            {
                Text = e.Name,
                Value = e.id.ToString()
            });
        }
        /// <summary>
        /// To Get Catogery From Entity Model Based On Current Lang
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SP_Catogery_Get_Result> BindCatogery()
        {
            aqwasDBEntities _aqwas = new aqwasDBEntities();
            var lang = (LanguageHelper.GetLang() == "ar-eg" ? 2 : 1);
            IEnumerable<SP_Catogery_Get_Result> _list = _aqwas.SP_Catogery_Get(lang);
            return _list;
        }
        #endregion

        #region RSS LinkS
        /// <summary>
        /// To Bind NewsPaper In DropDownList and Links In DropDownList IN UI
        /// </summary>
        /// <param name="CatogeryID"></param>
        /// <param name="NewsPaperID"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> GetLinksListForDropdownlist(int? CatogeryID, int? NewsPaperID)
        {
            IEnumerable<SP_FeedList_Get_Result> _list = BindLinks(CatogeryID, NewsPaperID);
            if (CatogeryID != null || NewsPaperID == null)
            {
                return _list.Select(e => new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.FeedListID.ToString()
                });
            }
            return _list.Select(e => new SelectListItem()
            {
                Text = e.link,
                Value = e.FeedListID.ToString()
            });
        }
        /// <summary>
        /// To Get NewsPaper & Links From Entity Model Based On Current Lang and Selected Catogery from ddl or newspaper in ddl
        /// </summary>
        /// <param name="CatogeryID"></param>
        /// <param name="NewsPaperID"></param>
        /// <returns></returns>
        public static IEnumerable<SP_FeedList_Get_Result> BindLinks(int? CatogeryID, int? NewsPaperID)
        {
            aqwasDBEntities _aqwas = new aqwasDBEntities();
            var lang = (LanguageHelper.GetLang() == "ar-eg" ? 2 : 1);
            IEnumerable<SP_FeedList_Get_Result> _list = _aqwas.SP_FeedList_Get(lang, CatogeryID, NewsPaperID);
            return _list;
        }
        /// <summary>
        /// To Save RSS NEWS IN DB BY Link of rss and NewsPaper ID to check if insert it before of not
        /// </summary>
        /// <param name="FeedID"></param>
        /// <param name="Link"></param>
        /// <returns></returns>
        public static string InsertRSSNewsInDB(string FeedID, string Link)
        {
            if (!string.IsNullOrEmpty(Link))
            {
                try
                {
                    var result = 0; // To Check If Any Url Not saved Success
                    aqwasDBEntities _aqwas = new aqwasDBEntities();
                    var reader = XmlReader.Create(Link);
                    var feed = SyndicationFeed.Load(reader);
                    reader.Close();

                    System.Data.Entity.Core.Objects.ObjectParameter is_succeed = new System.Data.Entity.Core.Objects.ObjectParameter("Is_success", typeof(long));
                    #region If RSS Exist IN DB Remove anf insert to update news 
                    System.Data.Entity.Core.Objects.ObjectParameter is_succeed_del = new System.Data.Entity.Core.Objects.ObjectParameter("Is_success", typeof(long));
                    _aqwas.SP_News_Check(((FeedID != "") ? (int.Parse(FeedID)) : (0)), is_succeed_del);
                    #endregion

                    foreach (SyndicationItem item in feed.Items)
                    {
                        string id = ((item != null && item.Id != null && item.Id.ToString() != "") ? (item.Id.ToString()) : (""));
                        string title = ((item != null && item.Title != null && item.Title.Text.ToString() != "") ? (item.Title.Text.ToString()) : (""));
                        string desc = ((item != null && item.Summary != null && item.Summary.Text.ToString() != "") ? (item.Summary.Text.ToString()) : (""));
                        DateTime PubDate = ((item != null && item.PublishDate != null && item.PublishDate.DateTime.ToString() != "") ? (item.PublishDate.DateTime) : (DateTime.Now));
                        string link = ((item != null && item.Links[0] != null && item.Links[0].Uri != null && item.Links[0].Uri.ToString() != "") ? (item.Links[0].Uri.ToString()) : (""));
                        //string path = item.Links[0].Uri.AbsoluteUri;
                        //string path2 = item.Links[0].Uri.OriginalString;
                        _aqwas.SP_News_Add(title, desc, link, PubDate, id, ((FeedID != "") ? (int.Parse(FeedID)) : (0)), is_succeed);
                        var res = ((is_succeed != null && is_succeed.Value != null && is_succeed.Value.ToString() != "") ? (int.Parse(is_succeed.Value.ToString())) : (0));
                        result += ((res > 0) ? (0) : (1));
                    }
                    return ((result == 0) ? ("1") : (result.ToString()));
                }
                catch (Exception ex)
                {
                    return "-1";
                }
            }
            else
                return "";
        }
        #endregion

        #region User
        /// <summary>
        /// To Check User Login If user Not Vaild error message will retrun in UserEmail in object
        /// </summary>
        /// <param name="userUName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static SP_User_Login_Result UserLogin(string userUName, string password)
        {
            aqwasDBEntities _aqwas = new aqwasDBEntities();
            var lang = (LanguageHelper.GetLang() == "ar-eg" ? 2 : 1);
            SP_User_Login_Result _data = _aqwas.SP_User_Login(userUName, password).FirstOrDefault();
            if (_data.UserID == -1)
            {
                return _data = new SP_User_Login_Result { UserID = _data.UserID, UserEmail = Resources.Aqwas.userval4 };
            }
            else if (_data.UserID == 0)
            {
                return _data = new SP_User_Login_Result { UserID = _data.UserID, UserEmail = Resources.Aqwas.userval1 };
            }
            else if (_data.UserID == -2)
            {
                return _data = new SP_User_Login_Result { UserID = _data.UserID, UserEmail = Resources.Aqwas.userval2 };
            }
            else if (_data.UserID == -3)
            {
                return _data = new SP_User_Login_Result { UserID = _data.UserID, UserEmail = Resources.Aqwas.userval3 };
            }
            return _data;
        }

        #endregion

        #region News
        /// <summary>
        /// To get recent new based  on date
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static SP_News_GetMostRecent_Result BindMostRecent(long? userid)
        {
            aqwasDBEntities _aqwas = new aqwasDBEntities();
            var lang = (LanguageHelper.GetLang() == "ar-eg" ? 2 : 1);
            SP_News_GetMostRecent_Result _list = _aqwas.SP_News_GetMostRecent(userid, lang).FirstOrDefault();
            return _list;
        }
        /// <summary>
        /// To retrieve news from only the subscribed providers  Based on Userid who login and Catid - Providerid - Pubdate - Newsid
        /// </summary>
        /// <param name="Userid"></param>
        /// <param name="Catid"></param>
        /// <param name="Providerid"></param>
        /// <param name="Pubdate"></param>
        /// <param name="Newsid"></param>
        /// <returns></returns>
        public static IEnumerable<SP_News_Search_Result> BindNews(long? Userid, int? Catid, int? Providerid, DateTime? Pubdate, long? Newsid)
        {
            aqwasDBEntities _aqwas = new aqwasDBEntities();
            var lang = (LanguageHelper.GetLang() == "ar-eg" ? 2 : 1);
            IEnumerable<SP_News_Search_Result> _list = _aqwas.SP_News_Search(lang, Userid, Catid, Providerid, Pubdate, Newsid);
            return _list;
        }
        #endregion
    }
}
