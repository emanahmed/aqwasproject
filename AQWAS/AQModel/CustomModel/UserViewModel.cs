﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AQModel.CustomeModel
{
    public class UserViewModel
    {
        [Required(ErrorMessageResourceName = "valusername", ErrorMessageResourceType = typeof(Resources.Aqwas))]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceName = "valpassword", ErrorMessageResourceType = typeof(Resources.Aqwas))]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public int RememberMe { get; set; }
    }
}
