﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using Utility;

namespace AqwasWebsite.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = LanguageHelper.GetLang();
            //UserData u = Login.sessionData();
            //if (u != null)
            //{
            //    u.defualt_lang_id = (cultureName == "ar-eg") ? 2 : 1;
            //}
            // Attempt to read the culture cookie from Request
            if (HttpContext.Request.Cookies["lang"] == null || HttpContext.Request.Cookies["lang"].Value == "")
            {
                HttpCookie lang_Cookie = new HttpCookie("lang", string.Empty);
                lang_Cookie.Expires = DateTime.Now.AddYears(1);
                HttpContext.Response.Cookies.Add(lang_Cookie);
                HttpContext.Request.Cookies["lang"].Value = lang_Cookie.Value = cultureName;
            }


            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }
        public string Change_Lang()
        {
            string lang = new LanguageHelper().Change_Lang();
            return lang;
        }
    }
}