﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Utility;

namespace AqwasWebsite.Controllers
{
    public class DefaultController : BaseController
    {
        // GET: Default
        public ActionResult Index()
        {
            ViewBag.Catogery = DAL.AqwasDAL.GetCategoryListForDropdownlist();
            return View();
        }
        [HttpPost]
        public JsonResult GetRSSLinks(int? CatogeryID, int? NewsPaperID)
        {
            try
            {
                var _data = DAL.AqwasDAL.GetLinksListForDropdownlist(CatogeryID, NewsPaperID);
                return Json(new { data = _data, Message = "success" });
            }
            catch (Exception)
            {
                return Json(new { data = "", Message = "Error" });
            }

        }
        public ActionResult SaveRSS(string Link)
        {
            string url = "https://arabic.arabianbusiness.com/feed/context/news/feed.xml";
            string url2 = "https://www.alarabiya.net/.mrss/ar.xml";
            string url3 = "https://www.alarabiya.net/.mrss/ar/sport.xml";
            string url4 = "https://sabq.org/rss";
            //string xml;
            //using (WebClient webClient = new WebClient())
            //{
            //    xml = Encoding.UTF8.GetString(webClient.DownloadData(url));
            //}
            //xml = xml.Replace("+00:00", "");
            //byte[] bytes = System.Text.UTF8Encoding.ASCII.GetBytes(xml);
            //XmlReader reader = XmlReader.Create(new MemoryStream(bytes));
            //SyndicationFeed feed = SyndicationFeed.Load(reader);

            //XmlReader r = new MyXmlReader(url);
            //SyndicationFeed feed = SyndicationFeed.Load(r);
            //Rss20FeedFormatter rssFormatter = feed.GetRss20Formatter();
            //XmlTextWriter rssWriter = new XmlTextWriter("rss.xml", Encoding.UTF8);
            //rssWriter.Formatting = Formatting.Indented;
            //rssFormatter.WriteTo(rssWriter);
            //rssWriter.Close();
            var data = "";
            using (var wc = new WebClient())
            {
                wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows; Windows NT 5.1; rv:1.9.2.4) Gecko/20100611 Firefox/3.6.4");
                data = wc.DownloadString(Link);
            }
            using (XmlReader reader = XmlReader.Create(new StringReader(data)))
            {
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                foreach (SyndicationItem item in feed.Items)
                {
                    String subject = item.Title.Text;
                    Console.WriteLine("{0}", subject);
                }
            }
            var reader2 = new XmlTextReader(new System.IO.StringReader(data));
            while (reader2.Read())
            {
                //
                SyndicationFeed feed = SyndicationFeed.Load(reader2);
            }



            //using (WebClient client = new WebClient())
            //{
            //    //set your proxy to client here
            //    string data = client.DownloadString(url);
            //    using (XmlReader reader = XmlReader.Create(new StringReader(data)))
            //    {
            //        SyndicationFeed feed = SyndicationFeed.Load(reader);
            //        foreach (SyndicationItem item in feed.Items)
            //        {
            //            String subject = item.Title.Text;
            //            Console.WriteLine("{0}", subject);
            //        }
            //    }
            //}


            // شغالة مع الكل مع 4 لينكات
            //var reader = XmlReader.Create(Link);
            //var feed = SyndicationFeed.Load(reader);
            //reader.Close();
            //foreach (SyndicationItem item in feed.Items)
            //{
            //    String subject = item.Title.Text;
            //    String summary = item.Summary.Text;
            //}
            return Json(new { data = "", Message = "Error" });

        }
        //public List<RssNews> Read(string url)
        //{
        //    var webClient = new WebClient();

        //    string result = webClient.DownloadString(url);

        //    XDocument document = XDocument.Parse(result);

        //    return (from descendant in document.Descendants("item")
        //            select new RssNews()
        //            {
        //                Description = descendant.Element("description").Value,
        //                Title = descendant.Element("title").Value,
        //                PublicationDate = descendant.Element("pubDate").Value
        //            }).ToList();
        //}
        public ActionResult Error()
        {
            return View();
        }
    }
}