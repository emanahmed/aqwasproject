﻿$('.changelang').on("click", function () { ChangeLan(); });
$('#ddlCatogry').change(function () {
    if (this.value != "") {
        BindData(this.value, null, $("#ddlNewspapername"));
    }
    else {
        $("#ddlNewspapername , #ddllinks").html($("<option     />").val("").text($('#hd_nodata').val()));
    }
});
$('#ddlNewspapername').change(function () {
    if (this.value != "") {
        BindData(null, this.value, $("#ddllinks"));
    }
    else {
        $("#ddllinks").html($("<option     />").val("").text($('#hd_nodata').val()));
    }
});
BindData = function (CatID, NewsNameID, ddlname) {
    var DTO = { 'CatogeryID': CatID, 'NewsPaperID': NewsNameID };
    CallServer('/Default/GetRSSLinks', JSON.stringify(DTO),
        function (response, status, xhr) {
            if (status == "success") {
                var Res = response.data;
                $(ddlname).html("");
                $(ddlname).append($("<option     />").val("").text($('#hd_select').val()));
                for (var i = 0; i < Res.length; i++) {
                    $(ddlname).append($("<option     />").val(Res[i].Value).text(Res[i].Text));
                }
            }
            else {
                toastr.error(data.Message);
            }

        });
}
$('#btnsave').click(function () {
    var Link = $("#ddllinks option:selected").text();
    if (Link == "") {
        toastr.warning($('#hd_valselect').val());
    }
    else {
        var DTO = { 'Link': Link };
        CallServer('/Default/SaveRSS', JSON.stringify(DTO),
            function (response, status, xhr) {
                debugger;
                if (status == "success") {
                    var Res = response.data;
                }
                else {
                    toastr.error(data.Message);
                }
            });
    }
})