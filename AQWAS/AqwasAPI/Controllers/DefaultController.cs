﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace AqwasAPI.Controllers
{
    public class DefaultController : BaseController
    {
        // GET: Default
        public ActionResult Index()
        {
            ViewBag.Catogery = DAL.AqwasDAL.GetCategoryListForDropdownlist();
            return View();
        }
        [HttpPost]
        public JsonResult GetRSSLinks(int? CatogeryID, int? NewsPaperID)
        {
            try
            {
                var _data = DAL.AqwasDAL.GetLinksListForDropdownlist(CatogeryID, NewsPaperID);
                return Json(new { data = _data, Message = "success" });
            }
            catch (Exception)
            {
                return Json(new { data = "", Message = "Error" });
            }

        }
        public ActionResult SaveRSS(string FeedID, string Link)
        {
            var res = DAL.AqwasDAL.InsertRSSNewsInDB(FeedID, Link);
            if (res == "-1")
            {
                return Json(new { data = res, Message = Resources.Aqwas.userval4 });
            }
            else if (res == "0")
            {
                return Json(new { data = res, Message = Resources.Aqwas.ErrorMsg });
            }
            return Json(new { data = res, Message = Resources.Aqwas.SuccessMsg });
        }
     
        public ActionResult Error()
        {
            return View();
        }
    }
}