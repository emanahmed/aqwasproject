﻿using AQModel;
using AQModel.CustomeModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AqwasAPI.Controllers
{
    public class HomeController : BaseController
    {
        #region News Part
        /// <summary>
        /// Home Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                SP_News_GetMostRecent_Result _data = new SP_News_GetMostRecent_Result();
                // If Session Is Not Null That User Login In system Get Email To View it in title
                if (Session["AqwasUser"] != null)
                {
                    var _obj = Session["AqwasUser"] as SP_User_Login_Result;
                    ViewBag.UserID = _obj.UserID;
                    ViewBag.UEmail = _obj.UserEmail;
                    _data = DAL.AqwasDAL.BindMostRecent(_obj.UserID);
                }
                else
                    return RedirectToAction(nameof(login));

                return View(_data);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Default");
            }
        }

        /// <summary>
        /// Get All News In All Providers In This Catogery 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult CatogryNews(int? Id)
        {
            try
            {
                IEnumerable<SP_News_Search_Result> _data = null;
                // If Session Is Not Null That User Login In system Get Email To View it in title
                if (Session["AqwasUser"] != null)
                {
                    var _obj = Session["AqwasUser"] as SP_User_Login_Result;
                    ViewBag.UserID = _obj.UserID;
                    ViewBag.UEmail = _obj.UserEmail;
                    if (Id != null && Id > 0)
                    {
                        ViewBag.Catid = Id;
                        _data = DAL.AqwasDAL.BindNews(_obj.UserID, Id, null, null, null);
                    }
                    else
                        return RedirectToAction("Error", "Default");
                }
                else
                    return RedirectToAction(nameof(login));

                return View(_data);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Default");
            }
        }
        /// <summary>
        /// Get news on specific Provider
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult providerNews(int? Id)
        {
            try
            {
                IEnumerable<SP_News_Search_Result> _data = null;
                // If Session Is Not Null That User Login In system Get Email To View it in title
                if (Session["AqwasUser"] != null)
                {
                    var _obj = Session["AqwasUser"] as SP_User_Login_Result;
                    ViewBag.UserID = _obj.UserID;
                    ViewBag.UEmail = _obj.UserEmail;
                    if (Id != null && Id > 0)
                    {
                        ViewBag.Npid = Id;
                        _data = DAL.AqwasDAL.BindNews(_obj.UserID, null, Id, null, null);
                    }
                    else
                        return RedirectToAction("Error", "Default");
                }
                else
                    return RedirectToAction(nameof(login));

                return View(_data);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Default");
            }
        }
        /// <summary>
        /// To Retrive Details Of Selected News (Read More)
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult NewDetails(int? Id)
        {
            try
            {
                SP_News_Search_Result _data = null;
                // If Session Is Not Null That User Login In system Get Email To View it in title
                if (Session["AqwasUser"] != null)
                {
                    var _obj = Session["AqwasUser"] as SP_User_Login_Result;
                    ViewBag.UserID = _obj.UserID;
                    ViewBag.UEmail = _obj.UserEmail;
                    if (Id != null && Id > 0)
                    {
                        ViewBag.Npid = Id;
                        _data = DAL.AqwasDAL.BindNews(_obj.UserID, null, null, null, Id).FirstOrDefault();
                    }
                    else
                        return RedirectToAction("Error", "Default");
                }
                else
                    return RedirectToAction(nameof(login));

                return View(_data);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Default");
            }

        }
        /// <summary>
        /// To Filter News Based on Catogery ID -  Provider id - - Datetime
        /// </summary>
        /// <param name="Catid"></param>
        /// <param name="Providerid"></param>
        /// <param name="Pubdate"></param>
        /// <returns></returns>
        public ActionResult NewsSearch()
        {
            try
            {
                IEnumerable<SP_News_Search_Result> _data = null;
                // If Session Is Not Null That User Login In system Get Email To View it in title
                if (Session["AqwasUser"] != null)
                {
                    var _obj = Session["AqwasUser"] as SP_User_Login_Result;
                    ViewBag.UserID = _obj.UserID;
                    ViewBag.UEmail = _obj.UserEmail;
                    ViewBag.Catogery = DAL.AqwasDAL.GetCategoryListForDropdownlist();
                    ViewBag.Newspaper = DAL.AqwasDAL.GetLinksListForDropdownlist(null, null);
                }
                else
                    return RedirectToAction(nameof(login));

                return View(_data);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Default");
            }
        }
        /// <summary>
        /// To Bind News After Search in div based on Catogery or NewsPaper or Datetime and which user Subscribe it 
        /// </summary>
        /// <param name="CatogeryID"></param>
        /// <param name="NewsPaperID"></param>
        /// <param name="Datetime"></param>
        /// <returns></returns>
        public ActionResult NewsSearchResult(int? CatogeryID, int? NewsPaperID, DateTime? Datetime)
        {
            try
            {
                if (Session["AqwasUser"] != null)
                {
                    var _obj = Session["AqwasUser"] as SP_User_Login_Result;
                    ViewBag.UserID = _obj.UserID;
                    ViewBag.UEmail = _obj.UserEmail;
                    IEnumerable<SP_News_Search_Result> _data = null;
                    _data = DAL.AqwasDAL.BindNews(_obj.UserID, CatogeryID, NewsPaperID, Datetime, null).ToList();
                    return View(_data);
                }
                else
                    return RedirectToAction(nameof(login));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Default");
            }
        }

        public JsonResult GetNewsSearchResult(int? CatogeryID, int? NewsPaperID, DateTime? Datetime)
        {
            try
            {
                if (Session["AqwasUser"] != null)
                {
                    var _obj = Session["AqwasUser"] as SP_User_Login_Result;
                    ViewBag.UserID = _obj.UserID;
                    ViewBag.UEmail = _obj.UserEmail;
                    IEnumerable<SP_News_Search_Result> _data = null;
                    _data = DAL.AqwasDAL.BindNews(_obj.UserID, CatogeryID, NewsPaperID, Datetime, null).ToList();
                    if (_data.Count() > 0)
                    {
                        return Json(new { data = _data, Message = "success" });
                    }
                    else
                        return Json(new { data = "0", Message = Resources.Aqwas.NoDataFound });
                }
                else
                    return Json(new { data = "-5", Message = "Error" });
            }
            catch (Exception)
            {
                return Json(new { data = "-1", Message = "Error" });
            }
        }

        #endregion

        #region Login Part
        public ActionResult login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult loginPOST(UserViewModel user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var _obj = DAL.AqwasDAL.UserLogin(user.UserName, user.Password);
                    if (_obj.UserID > 0)
                    {
                        Session["AqwasUser"] = _obj;
                        return Json(new { message = "" });
                    }
                    else
                        return Json(new { message = _obj.UserEmail });
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Default");
                }
            }
            else
            {
                return Json(new { message = ((Resources.Aqwas.Lang == "ar") ? ("يرجي إكمال البيانات") : ("Please Complete Data")) });
            }
        }
        /// <summary>
        /// To Log out and clear session -  cookie if exist
        /// </summary>
        /// <returns></returns>
        public ActionResult logout()
        {
            if (System.Web.HttpContext.Current.Session["AqwasUser"] != null)
            {
                System.Web.HttpContext.Current.Session["AqwasUser"] = null;
                System.Web.HttpContext.Current.Session.Remove("AqwasUser");
            }
            // To Remove Session From Memory
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.Abandon();
            System.Web.HttpContext.Current.Session.RemoveAll();
            System.Web.HttpContext.Current.User = null;
            return RedirectToAction(nameof(login));
        }
        #endregion
    }
}
