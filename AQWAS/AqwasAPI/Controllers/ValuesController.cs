﻿using AQModel;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AqwasAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values/GetItems
        /// <summary>
        /// To Get All Catogery
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/values/GetCatogeryAPI")]
        public IEnumerable<SP_Catogery_Get_Result> GetCatogeryAPI()
        {
            return AqwasDAL.BindCatogery();
        }
        /// <summary>
        /// To Get RSS Link Based On Catogery - Provider
        /// </summary>
        /// <param name="CatogeryID"></param>
        /// <param name="NewsPaperID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/values/GetLinksAPI")]
        public IEnumerable<SP_FeedList_Get_Result> GetLinksAPI(int? CatogeryID, int? NewsPaperID)
        {
            return AqwasDAL.BindLinks(CatogeryID, NewsPaperID);
        }
        /// <summary>
        /// Used To User Login 
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Pass"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/values/UserLoginAPI")]
        public SP_User_Login_Result UserLoginAPI(string UserName, string Pass)
        {
            return AqwasDAL.UserLogin(UserName, Pass);
        }
        /// <summary>
        /// To Save News In RSS in db
        /// </summary>
        /// <param name="FeedID"></param>
        /// <param name="Link"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/values/AddRSSAPI")]
        public string AddRSSAPI(string FeedID, string Link)
        {
            return AqwasDAL.InsertRSSNewsInDB(FeedID, Link);
        }
        /// <summary>
        /// To Get Recent New based on user login
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/values/UserMostRecentNewAPI")]
        public SP_News_GetMostRecent_Result UserMostRecentNewAPI(long? userid)
        {
            return AqwasDAL.BindMostRecent(userid);
        }
        /// <summary>
        /// To retrieve news from only the subscribed providers  Based on Userid who login and Catid - Providerid - Pubdate - Newsid
        /// </summary>
        /// <param name="Userid"></param>
        /// <param name="Catid"></param>
        /// <param name="Providerid"></param>
        /// <param name="Pubdate">2019-11-12 WITH FORMATE (YYYY-MM-DD)</param>
        /// <param name="Newsid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/values/UserNewSAPI")]
        public IEnumerable<SP_News_Search_Result> UserNewSAPI(long? Userid, int? Catid, int? Providerid, DateTime? Pubdate, long? Newsid)
        {
            return AqwasDAL.BindNews(Userid, Catid, Providerid, Pubdate, Newsid);
        }
        //// GET api/values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //public void Delete(int id)
        //{
        //}
    }
}
