﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace AqwasAPI.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = LanguageHelper.GetLang();
            
            // Attempt to read the culture cookie from Request
            if (HttpContext.Request.Cookies["lang"] == null || HttpContext.Request.Cookies["lang"].Value == "")
            {
                HttpCookie lang_Cookie = new HttpCookie("lang", string.Empty);
                lang_Cookie.Expires = DateTime.Now.AddYears(1);
                HttpContext.Response.Cookies.Add(lang_Cookie);
                HttpContext.Request.Cookies["lang"].Value = lang_Cookie.Value = cultureName;
            }


            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }
        public string Change_Lang()
        {
            return new LanguageHelper().Change_Lang();
        }
    }
}