﻿//#region Event
// Button Of Change Lang
$('.changelang').on("click", function () { ChangeLan(); });
// Change DDL Of Catogery To Bind DDL Of Newspaper
$('#ddlCatogry').change(function () {
    if (this.value != "") {
        BindData(this.value, null, $("#ddlNewspapername"));
    }
    else {
        $("#ddlNewspapername , #ddllinks").html($("<option     />").val("").text($('#hd_nodata').val()));
    }
});
// Change DDL Of Newspaper To Bind RSS Links
$('#ddlNewspapername').change(function () {
    if (this.value != "") {
        BindData(null, this.value, $("#ddllinks"));
    }
    else {
        $("#ddllinks").html($("<option     />").val("").text($('#hd_nodata').val()));
    }
});
//  Button That Show Or Hide Div Of Manage import News In RSS in Database
$('#btnimport').click(function () {
    if ($('#div_import').hasClass('hidden') == true) {
        $('#div_import').removeClass('hidden');
    }
    else {
        $('#div_import').addClass('hidden');
    }
});
// Button Save News In RSS in Database
$('#btnsave').click(function () {
    var Link = $("#ddllinks option:selected").text();
    var ID = $("#ddlNewspapername").val();
    if ($("#ddllinks").val() == "" || ID == "") {
        toastr.warning($('#hd_valselect').val());
    }
    else {
        $('#loader-overlay').show();
        var DTO = { 'FeedID': ID, 'Link': Link };
        CallServer('/Default/SaveRSS', JSON.stringify(DTO),
            function (response, status, xhr) {
                $('#loader-overlay').hide();
                if (response.data > 0) {
                    toastr.success(response.Message);
                }
                else {
                    toastr.error(response.Message);
                }
                $('#div_import').addClass('hidden');
                $('#ddlCatogry').val("");
                $("#ddlNewspapername , #ddllinks").html($("<option     />").val("").text($('#hd_nodata').val()));
            });
    }
})

//#endregion


//#region Function
// To Bind DDL Of News Paper & Bind DDL oF RSS Links
BindData = function (CatID, NewsNameID, ddlname) {
    var DTO = { 'CatogeryID': CatID, 'NewsPaperID': NewsNameID };
    CallServer('/Default/GetRSSLinks', JSON.stringify(DTO),
        function (response, status, xhr) {
            if (status == "success") {
                var Res = response.data;
                $(ddlname).html("");
                $(ddlname).append($("<option     />").val("").text($('#hd_select').val()));
                for (var i = 0; i < Res.length; i++) {
                    $(ddlname).append($("<option     />").val(Res[i].Value).text(Res[i].Text));
                }
            }
            else {
                toastr.error(data.Message);
            }

        });
}
//#endregion