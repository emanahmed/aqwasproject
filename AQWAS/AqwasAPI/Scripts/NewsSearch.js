﻿//#region Event
$(function () {
    // This will make every element with the class "date-picker" into a DatePicker element
    $('.date-picker').datepicker({ dateFormat: 'yy-mm-dd' });
});
// Change DDL Of Catogery To Bind DDL Of Newspaper
$('#ddlCatogry').change(function () {
    if (this.value != "") {
        BindData(this.value, $("#ddlNewspapername"));
    }
    else {
        $("#ddlNewspapername").html($("<option     />").val("").text($('#hd_nodata').val()));
    }
});
// To Search based on search Catogery and bind data in div
$('#btnsearch').click(function () {
    var catddl = $("#ddlCatogry").val();
    var Newspaperddl = $("#ddlNewspapername").val();
    var Date = $.trim($("#txtNewsdate").val());
    window.location.href = "/Home/NewsSearchResult?CatogeryID=" + catddl + "&NewsPaperID=" + Newspaperddl + "&Datetime=" + Date + "";
});
// To Clear Data To Reset Search
$('#btnclear').click(function () {
    $("#ddlCatogry").val('');
    $("#ddlNewspapername").val('');
    $("#txtNewsdate").val('');
});
//#endregion


//#region Function
// To Bind DDL Of News Paper & Bind DDL oF RSS Links
BindData = function (CatID, ddlname) {
    var DTO = { 'CatogeryID': CatID, 'NewsPaperID': null };
    CallServer('/Default/GetRSSLinks', JSON.stringify(DTO),
        function (response, status, xhr) {
            if (status == "success") {
                var Res = response.data;
                $(ddlname).html("");
                $(ddlname).append($("<option     />").val("").text($('#hd_select').val()));
                for (var i = 0; i < Res.length; i++) {
                    $(ddlname).append($("<option     />").val(Res[i].Value).text(Res[i].Text));
                }
            }
            else {
                toastr.error(data.Message);
            }
        });
}
//#endregion
