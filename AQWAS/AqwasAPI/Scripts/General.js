﻿// For Ajax call In Mvc Way
// URL >> Path of controller/action 
// data >> Data Will send
// successFunction >> function to do after success what you want
// errorFunction >> function to redirect to login if error occur 
// datatype >> "json","html" >>> any type if thia Attribute == null >> with take default value >> json
CallServerMvc = function (url, data, successFunction, errorFunction, async) {
    $.ajax({
        url: url,
        data: data,
        async: ((async != undefined && async != null) ? (async) : (false)),
        success: successFunction,
        error: (errorFunction == null && errorFunction == undefined) ? (function (d) {
            window.location.href = "/Default/Error";
        }) : (errorFunction)
    });
}
// For Ajax call General 
// URL >> Path of controller/action 
// data >> Data Will send
// successFunction >> function to do after success what you want
// errorFunction >> function to redirect to login if error occur 
// datatype >> "json","html" >>> any type if this Attribute == null >> with take default value >> json
CallServer = function (url, data, successFunction, errorFunction, datatype, async) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: ((datatype != undefined && datatype != null) ? (datatype) : ("json")),
        async: ((async != undefined && async != null) ? (async) : (false)),
        success: successFunction,
        error: (errorFunction == null && errorFunction == undefined) ? (function (d) {
            debugger;
            window.location.href = "/Default/Error";
        }) : (errorFunction)
    });
}
ChangeLan = function () {
    CallServerMvc('/Base/Change_Lang', '{}',
        function (response) {
            location.reload();
        });
}