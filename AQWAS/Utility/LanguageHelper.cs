﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources;
using System.Web;

namespace Utility
{
    public class LanguageHelper : System.Web.SessionState.IRequiresSessionState
    {
        public static List<Languages> AvailableLanguages = new List<Languages> {
            new Languages {
                LanguageFullName =Aqwas.Lang , LanguageCultureName = Aqwas.Lang
            },
            new Languages {
                LanguageFullName = Aqwas.Lang, LanguageCultureName =Aqwas.Lang
            },
        };
        public static bool IsLanguageAvailable(string lang)
        {
            return AvailableLanguages.Where(a => a.LanguageCultureName.Equals(lang)).FirstOrDefault() != null ? true : false;
        }
        public static string GetDefaultLanguage()
        {
            return AvailableLanguages[0].LanguageCultureName;
        }
        public int GetCookieLang()
        {
            return ((HttpContext.Current.Request.Cookies["lang"] != null && !string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["lang"].Value.ToString())) ? ((HttpContext.Current.Request.Cookies["lang"].Value.ToString() == "ar-eg") ? (2) : (1)) : (2));
        }
        public static string GetLang()
        {
            string lang =
                (HttpContext.Current.Request.Cookies["lang"] != null &&
                 !string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["lang"].Value.ToString()))
                    ? (HttpContext.Current.Request.Cookies["lang"].Value.ToString())
                    : (string.Empty);
            return ((lang == string.Empty || lang == "ar-eg") ? "ar-eg" : "en-US");

        }

        public string Change_Lang()
        {
            string lang = GetLang();
            HttpCookie lang_Cookie = new HttpCookie("lang", string.Empty);
            lang_Cookie.Expires = DateTime.Now.AddYears(1);
            HttpContext.Current.Response.Cookies.Add(lang_Cookie);
            if (string.IsNullOrEmpty(lang) || lang == "ar-eg")
            {
                lang_Cookie.Value = HttpContext.Current.Request.Cookies["lang"].Value = lang = "en-US";
            }
            else
            {
                lang_Cookie.Value = HttpContext.Current.Request.Cookies["lang"].Value = lang = "ar-eg";
            }
            return lang;
        }

    }
    public class Languages
    {
        public string LanguageFullName { get; set; }
        public string LanguageCultureName { get; set; }

    }
}
